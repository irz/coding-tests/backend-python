# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

-

### Changed

-

### Deprecated

-

### Removed

-

### Fixed

-

### Security

-

## [1.1.1] - 2022-11-09

### Fixed

- CHANGELOG.md markdown error

## [1.1.0] - 2022-11-08

### Changed

- Formulation of the test task

## [1.0.0] - 2022-03-21

### Added

- Coding test

[Unreleased]: https://gitlab.com/irz/coding_tests/backend_python/-/tree/develop

[1.1.1]: https://gitlab.com/irz/coding_tests/backend_python/-/tags/v1.1.1

[1.1.0]: https://gitlab.com/irz/coding_tests/backend_python/-/tags/v1.1.0

[1.0.0]: https://gitlab.com/irz/coding_tests/backend_python/-/tags/v1.0.0
