# Тестовое задание

## Модифицируйте приложение согласно следующим требованиям:

- Создайте метод `generate_list` класса `Test`, находящегося в [model.py], принимающий на вход `int` и
  возвращающий `generator` заданного количества значений в диапазоне от 0 до 99
- Создайте метод `handle_get_generate_list` класса `TestHandler`, находящегося в [\_\_init__.py], принимающий на
  вход `dict` содержащий ключ `index` и значение `int`, вызывающий `generate_list` и возвращающий `list`
  уникальных значений сгенерированных `generate_list`
- Создайте метод `get_generate_list` класса `TestApi`, находящегося в [api.py], принимающий на вход `int`,
  содержащий [RPC] `handle_get_generate_list` и возвращающий результат [RPC] `handle_get_generate_list`
- Создайте *endpoint* `get` для *router* `test_router`, находящегося в [router.py], принимающий на вход `int`,
  содержащий вызов `get_generate_list` и возвращающий результат вызова `get_generate_list`

### Примечания:

- [RPC]\: *https://en.wikipedia.org/wiki/Remote_procedure_call*
- Для работы приложения необходим [Redis], порт подключения настраивается в файле [settings.py]
- С вопросами можете обращаться по контактным данным указанным в файле [settings.py]

#### Общие требования:

- Проект необходимо выложить в открытый репозиторий на GitLab
- Комментарии необходимо писать на английском языке
- Допускается использование любых фреймворков и библиотек
- Используйте последние актуальные версии фреймворков, библиотек и иных средств разработки

[model.py]: https://gitlab.com/irz/coding_tests/backend_python/-/blob/main/services/test/model.py

[\_\_init__.py]: https://gitlab.com/irz/coding_tests/backend_python/-/blob/main/services/test/__init__.py

[api.py]: https://gitlab.com/irz/coding_tests/backend_python/-/blob/main/services/test/api.py

[router.py]: https://gitlab.com/irz/coding_tests/backend_python/-/blob/main/services/test/router.py

[settings.py]: https://gitlab.com/irz/coding_tests/backend_python/-/blob/main/settings.py

[RPC]: https://www.jsonrpc.org/specification

[Redis]: https://redis.io/